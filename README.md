# GraphQL
É uma linguagem de consulta de dados, que funciona em cima do protocolo HTTP, porém ela cria algumas regras em cima de como as informações serão transitadas entre o frontend e o backend.

## Padrão REST x GRAPHQL
- REST tem que fazer uma requisição para cada dado.
- GraphQL com apenas uma requisição, você já traz todos os dados necessários, trazendo muito mais performance dependendo da aplicação.
- GraphQL surgiu decorrente do under-fetch e over-fetch, ou seja, quando solicitado dados de uma API, ou eles retornavam pouco ou muitos dados.
- GraphQL é basicamente uma linguagem de consulta e mutação de dados de API.
- Dá pra usar GraphQL sem nem ter banco de dados.
- Ela permite a definição de um Schema, um modelo de dados.
- Ela formaliza a sua API.
- Cria uma consulta pedindo exatamente o que você quer.

## Rotas

### API REST
- GET - /users
- POST - /users
- PUT - /users/:id
- DELETE - /users/:id

### GraphQL
Possui apenas uma rota que geralmente se chama graphql e aceita o método POST.

Uma consulta no GraphQL é um objeto JSON que no corpo da requisição ficam as querys.

No GraphQL não temos todos os métodos que temos no REST, para informar o tipo de operação que queremos fazer, possuímos apenas 3, que são:
query - Buscando informação (Nunca altera nada no backend).
mutation - Alterar, criar ou deletar uma informação.
subscription - Toda vez que quiser ouvir uma informação em tempo real.

*Obs:* Bibliotecas GraphQL já possuem realtime integrada, tirando a necessidade de usar um socket.io por exemplo.

## Exemplo:
```
{
	query todosUsuarios {
		users()
	}

	query todosPedidos {
		users()
	}
}
```

## Frontend
No GraphQL, a gente passa mais responsabilidades pro frontend, é o frontend que determina como os dados serão exibidos.

## Documentação
O GraphQL cria a documentação de forma automática do backend, tirando a necessidade de usar ferramentas como Swagger para fazer a documentação.

Isso é possível porque o GraphQL por meio do Schema guarda a tipagem das nossas rotas e do retorno delas.

## Frameworks
- graphql-yoda é um wrapper em volta do express e de outros frameworks.
