const { GraphQLServer } = require('graphql-yoga')
const path = require('path')
const resolvers = require('./resolvers')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:45167/nodeql')

const server = new GraphQLServer({
    //Referênciando a schema
    typeDefs: path.resolve(__dirname, 'schema.graphql'),

    //Dentro do GraphQL, controllers são chamados de resolvers
    resolvers
})

//Ao executar o servidor, ele abre uma especie de insomnia para GraphQL
server.start()