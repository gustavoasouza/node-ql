const User = require('./User')

module.exports = {
    Query: {
        //Rota users retornando users
        //Graphql já espera que seja retornado uma promise, por isso não precisa usar async
        users: () => User.find(), 
        //Retornando o primeiro usuário
        user: (_, { id }) => User.findById(id)
    },

    Mutation: {
        createUser: (_, { name, email }) => User.create({ name, email })
    }
}